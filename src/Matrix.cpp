#include "Matrix.hh"
#include <iostream>
#include <cmath>




/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy Matrix, ktore zawieraja wiecej kodu
 *  niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */
std::ostream &operator<<(std::ostream &stream,  Matrix &matrix)
{
    for(int x = 0; x<SIZE; x++)
    {
        stream << matrix[x] << std::endl;
        
    }
    return stream;
}

Vector operator*(Matrix Mat, Vector Vec)
{
    Vector result;
    for(int x = 0; x<SIZE; x++)
    {
        for(int y = 0; y<SIZE; y++)
        {
           result[x] += Mat[x][y] * Vec[x];
        }
    }
    return result;
}

Matrix tran(Matrix Mat) //macierz transponowana
{
    Matrix trans;
    for (int x = 0; x<SIZE ; x++)
    {
        for (int y = 0; y<SIZE ; y++)
        {
            trans[y][x] = Mat[x][y];
        }
    }
   return trans;

}
Matrix Matrix::operator*(const Matrix& m)const
{
    Matrix result;
    Matrix mat = m; // macierz pomocnicza
    tran(m);   // transponuje macierz, ułatwienie mnożenia
    for (int x = 0; x<SIZE; x++)
    {
        for (int y = 0; y<SIZE; y++)
        {
            result.Row[x][y] = Row[x] * m.Row[y];
        }
    }
    return result;
}