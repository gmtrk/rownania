#include "Vector.hh"
#include <iostream>

using namespace std;

/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy Vector, ktore zawieraja wiecej kodu
 *  niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

ostream &operator<<(ostream &stream,  Vector &vec)
{
    

    for(int x = 0; x<SIZE; x++)
    {
      stream << vec[x] << " ";
    }
    return stream;
}

istream &operator>>(std::istream &stream, Vector &vec)
{
for(int x = 0; x<SIZE; x++)
{
  stream >> vec[x];
  while (isspace(stream.peek())) //jezeli znakiem jest spacja to jest ignorowana
    {
        stream.ignore();
    }
}
  return stream;
}
Vector operator + (Vector A, Vector B)
{
  Vector result;
  
  
  for(int x = 0; x<SIZE; x++)                 
  {
    result[x] = A[x] + B[x];
  }

  return result;
}

Vector operator - (Vector A, Vector B)
{
  Vector result;
  
  
  for(int x = 0; x<SIZE; x++)                 
  {
    result[x] = A[x] - B[x];
  }

  return result;
}
double operator * (Vector A, Vector B) // wektor skalarny
{
  double temp;   //zmienna tymczasowa, przechowuje dotychczasowe obliczenia
  double result;
  
  for(int x = 0; x<SIZE; x++)                 
  {
    temp= A[x] * B[x];
    result += temp;
  }

  return result;
}
Vector operator * (Vector A, double num)
{
 Vector result;
  
  
  for(int x = 0; x<SIZE; x++)                 
  {
    result[x] = A[x] * num;
  }

  return result;
}