
#include "SystemOfLinearEquations.hh"
#include <math.h>
using namespace std;

/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy SystemOfLinearEquations, ktore zawieraja 
 *  wiecej kodu niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */
std::ostream &operator<<(std::ostream &stream, SystemOfLinearEquations &system)
{
    for (int x = 0; x<SIZE; x++)
    {
        if (x == (SIZE/2 - 0.5) || x == SIZE/2)
        {
            stream << "|" << system.matrix[x] << "| " << "|x_" << x+1 << "| = " << "|" << system.vector[x] << "|" << std::endl;
        }
        else
        {
            stream << "|" << system.matrix[x] << "| " << "|x_" << x+1 << "|   " << "|" << system.vector[x] << "|" << std::endl;
        }
    }
    return stream;
}


std::istream &operator>>(std::istream &stream, SystemOfLinearEquations &system)
{
    for (int x =0; x<SIZE; x++)
    {
        stream >> system.matrix[x];
    }
    stream >> system.vector;

    return stream;
}



bool load (string name, SystemOfLinearEquations sys)
{
    ifstream file;
    double wyznacznik;
    file.open(name.c_str());
    if(!file.good()){return false;}
    while (true)
    {
        file >> sys;
        if (!file.fail())
        {
            SystemOfLinearEquations trans = sys;
            trans.matrix = sys.trans();
            
            cout << "Twoje rownanie: "<< endl <<trans;
            wyznacznik = sdet(sys);
            cout << "Wyznacznik: " << wyznacznik <<endl;
        }
        else {break;}
    }
    return true;
}

double sdet(SystemOfLinearEquations sys)
{   

    // jezeli pierwszy element zerowy, dodaj nastepny niezerowy z wierszy nizej
    for (int x = 0; x < SIZE; x++)
    {
        if (std::abs(sys.matrix[x][x]) < EPS)
        {
            for (int k = 0; k < SIZE; ++k)
            {
                if (std::abs(sys.matrix[k][x]) >= EPS)
                {
                    sys.matrix[x] = sys.matrix[x] + sys.matrix[k];
                    sys.vector[x] = sys.vector[x] + sys.vector[k];
                    break;
                }
            }
        }
    }

    // zerowanie wierszy nizej, tworzenie macierzy trojkatnej
    for (int c = 0; c < SIZE; c++)
    {
        if (std::abs(sys.matrix[c][c]) >= EPS)
        {
            for (int r = c + 1; r < SIZE; ++r)
            {
                double factor = sys.matrix[r][c] / sys.matrix[c][c];
                sys.matrix[r] = sys.matrix[r] - sys.matrix[c] * factor;
                sys.vector[r] = sys.vector[r] - sys.vector[c] * factor;

            }
        }
    }

    // liczenie wyznacznika macierzy
    double det = 1;
    for (int x = 0; x < SIZE; ++x)
    {
        det = sys.matrix[x][x] * det;
    }

    return det;
}



double solve (string name, SystemOfLinearEquations sys)
{
    ifstream file;
    Vector result, error; // tablica niewiadomych i bledu
    SystemOfLinearEquations systemp; //zmienna tymczasowa, przechowuje kopie
    double det, dettemp; //zmienna wyznacznik oraz tymczasowa do wyliczania
    file.open(name.c_str());

    file >> sys;
    det = sdet(sys);
    cout << "Rozwiazania ukladu rownan: " <<endl;
    if (det == 0)
    {
        cout << "Wyznacznik rowny 0, brak rozwiazan, nieskonczona ilosc";
    }
    else
    {
    for (int x = 0; x<SIZE; x++)
    {
        systemp = sys;
        systemp.matrix[x] = systemp.vector;
        dettemp = sdet(systemp);
        result[x] = dettemp/det;

        cout << "x_" << x+1 << " = " << result[x] << "  ";
    }
    error = sys.matrix*result - sys.vector;
        cout << endl;
        cout << "Wektor bledu:             Ax-b   = (" << error  << ")" << endl;
        
        cout << "Dlugosc wektora bledu : ||Ax-b|| = "<< sqrt(error*error);
    }
    return 0;
}