#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include "SystemOfLinearEquations.hh"

using namespace std;

/*
 * Tu definiujemy pozostale funkcje.
 * Lepiej jednak stworzyc dodatkowy modul
 * i tam je umiescic. Ten przyklad pokazuje
 * jedynie absolutne minimum.
 */

int main()
{
  SystemOfLinearEquations system; // To tylko przykladowe definicje zmiennej
  
  string name;

  cout << endl
       << " Start programu " << endl
       << endl;

cout << "Podaj nazwe pliku w formacie 'nazwa.txt'" << endl;
cin >> name;
if (load(name, system) != 1)
{
  cout << "Nie udalo sie otworzyc pliku" << endl;
}
else
{
  solve (name, system);
}



}


/****************************************************************************************************
 * Jakub Gmiterek PO Pn 17:05 Układy równań liniowych                                               *
 * Program kompiluje się bez problemów.                                                             *
 * Przy starcie prosi mnie o podanie nazwy pliku z danymi,                                          *
 * więc podaje rownanie.txt, ktęre zawiera dane ze strony dr Kreczmera.                             *
 * Otrzymuje poprawnie ułożony układ, wyznacznik macierzy tego układu, oraz wyniki i błędy          *
 * Zmieniam dane, wpisując w losowe miejsca zera, sprawdzam czy algorytm będzie działał poprawnie.  *
 * Jedynie nie w każdym wypadku funkcja wykazująca długość wektora błędu zwróci liczbę,             *
 * ponieważ wychodzi ona poza zakres.                                                               *
 * Uważam że program działa dobrze i spełnia wymagania zadania                                      *
 ****************************************************************************************************/