#pragma once

#include "Size.hh"
#include <iostream>

/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class Vector
{
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
  double vx [SIZE]; //tablica wyrazów wolnych

public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */
  const double& operator [] (int idx) const {return vx[idx];}
  double& operator[](int idx) { return vx[idx]; } // funkcja pobierająca wyrazy wolne z prywatnej części
 
};

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::istream &operator>>(std::istream &stream, Vector &vec);

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::ostream &operator<<(std::ostream &stream, Vector &vec);

Vector operator + (Vector A, Vector B);
Vector operator - (Vector A, Vector B);
double operator * (Vector A, Vector B);
Vector operator * (Vector A, double x);

