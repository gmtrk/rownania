#pragma once

#include "Size.hh"
#include "Vector.hh"
#include <iostream>
#define EPS 0.0001 // zdefiniowana zmienna, zapobiegająca errorom
/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class Matrix
{
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
  Vector Row[SIZE];
public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */

const Vector& operator[ ] (int size) const { return Row[size]; }
Vector& operator[ ] (int size) { return Row[size]; }
Matrix operator * (const Matrix& m) const;


  
};

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::istream &operator>>(std::istream &stream, Matrix &matrix);

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::ostream &operator<<(std::ostream &stream,  Matrix &matrix);

Vector operator*(Matrix Mat, Vector Vec); // mnożenie macierzy przez wektor

Matrix tran(Matrix Mat);
