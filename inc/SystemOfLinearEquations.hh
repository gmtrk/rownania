#pragma once

#include "Matrix.hh"
#include <fstream>
#include <iostream>


/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class SystemOfLinearEquations
{
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
  friend std::ostream &operator<<(std::ostream &stream, SystemOfLinearEquations &system);
  friend std::istream &operator>>(std::istream &stream, SystemOfLinearEquations &system);
  friend double solve (std::string name, SystemOfLinearEquations sys);
  friend bool load (std::string name, SystemOfLinearEquations sys);
  friend double sdet (SystemOfLinearEquations sys);
  Matrix matrix;
  Vector vector; //wyrazy wolne
public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */
const Vector& operator[ ] (int size) const { return matrix[size]; }
Vector& operator[ ] (int size) { return matrix[size]; }
double term (int idx) const {return vector[idx];}
double& term (int idx) { return vector[idx]; }
Matrix trans () {return tran(matrix);}



};

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::istream &operator>>(std::istream &stream, SystemOfLinearEquations &system);

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::ostream &operator<<(std::ostream &stream, SystemOfLinearEquations &system); 
bool load (std::string name, SystemOfLinearEquations sys);
double sdet (SystemOfLinearEquations sys);

